package com.company;

public class readyStock extends Kue {

    private double jumlah;
    public readyStock(String nama, double harga, double jumlah) {

        super(nama, harga);

        this.jumlah = jumlah;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public double hitungHarga() {
        return getPrice() * jumlah * 2;
    }

    @Override
    public double Berat() {
        return 0;
    }

    @Override
    public double Jumlah() {
        return hitungHarga();
    }

}

